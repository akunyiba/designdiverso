<?php
require_once(dirname(__FILE__) . "/VerifyEmail.php");

if ($_POST) {
    // Initialize mail library class
    $mail = new VerifyEmail();

    // Set the timeout value on stream
    $mail->setStreamTimeoutWait(1);

    // Set debug output mode
    $mail->Debug= TRUE; 

    // Set email address for SMTP request
    $mail->setEmailFrom('from@email.com');

    $email = $_POST['email'] ;
    // Check if email is valid and exist
    echo json_encode($mail->check($email)) ;
}