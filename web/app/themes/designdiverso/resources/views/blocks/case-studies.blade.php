{{--
  Title: Case Studies
  Description: Case Studies
  Category: dd-blocks
  Icon: portfolio
  Keywords: case study, case studies, portfolio
  Mode: preview
  Align: wide
  PostTypes: page post
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $block_id = 'dd-case-studies-item-' . $block['id'];
  if( !empty($block['anchor']) ) {
      $block_id = $block['anchor'];
  }

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-case-studies';
  if( !empty($block['className']) ) {
      $className .= ' ' . $block['className'];
  }
  if( !empty($block['align']) ) {
      $className .= ' align' . $block['align'];
  }

  // Load values and assing defaults.
  $filter_categories = get_terms(array(
      'taxonomy' => 'case-study-category',
      'hide_empty' => false,
  ));
  $args = array(
          'post_type' => 'case-study',
          'posts_per_page' => -1
      );
  $case_studies_query = new WP_Query($args);
@endphp

<section id="{{ esc_attr($block_id) }}" data-{{ esc_attr($block_id) }} class="{{ esc_attr($className) }}">

  @if( $filter_categories )
  <div class="dd-case-studies-filter-container cl-effect-4">
  @foreach ($filter_categories as $category )
    <a
    class="dd-case-studies-filter-link {{ !$loop->first ? 'dd-case-studies-filter-link--separator' : '' }}"
    href="javascript:;"
    data-filter="{{ $category->slug }}">{{ $category->name }}</a>
  @endforeach
  </div>
  @endif

  <div class="flex flex-wrap">
    @while ($case_studies_query->have_posts())
      @php
        $case_studies_query->the_post();

        $case_study_id = get_the_ID();
        $title = get_the_title();
        $image = get_the_post_thumbnail_url();
        $categories = get_the_terms( $case_study_id, 'case-study-category' );
        $category_singular_name = $categories[0]->description;
        $link = get_the_permalink();
        $logo = '';
        $logo_url = '';
        $logo_alt = '';
        $category_classes = '';

        if(get_field('logo', $case_study_id)) {
          $logo = get_field('logo', $case_study_id);
          $logo_url = $logo['url'];
          $logo_alt = $logo['alt'];
        }
      @endphp
      @if( is_array($categories) )
        @foreach ( $categories as $category )
          @php $category_classes .= $category->slug.' '; @endphp
        @endforeach
      @endif
      <div x-data="{hover: false}" class="dd-case-study-item dd-case-study-item--visible {{ $category_classes }}">
        <a @mouseover="hover = !hover" @mouseout="hover = !hover" href="{{ esc_url($link) }}" class="dd-case-study-item__inner border-none text-black hover:text-primary overflow-hidden">
          <h3 class="dd-anime-slide-up">{{ $title }}</h3>
          <figure class="dd-anime-image relative">
            <img src="{{ $image }}" alt="{{ $title }}" class="dd-case-study-item__image">
            @if($logo)
              <div :class="{ 'opacity-75': hover, 'opacity-0': !hover}"" class="flex items-center justify-center absolute w-full h-full bg-black top-0 left-0 transition ease-in duration-300">
                <img src="{{  $logo_url }}" alt="{{ $logo_alt }}" class="w-full h-auto px-10">
              </div>
            @endif
          </figure>
          <span class="dd-case-study-item__link" href="{{ esc_url($link) }}">{{ __('View ', 'sage') . $category_singular_name }}</span>
        </a>
      </div>
    @endwhile
  </div>
</section>

<script>

(function($) {
  $('.dd-case-study-item:odd').each(function (i) {
      $(this).addClass('dd-case-study-item--margin');
  });

  $(function(){
    $('.dd-case-studies-filter-link').click(function(){
      var value = $(this).attr('data-filter');
      var items = $('.dd-case-study-item');
      var match = items.filter('.'+value);

      items.not('.'+value).removeClass('dd-case-study-item--margin dd-case-study-item--visible').hide(400);
      match.filter(':odd').addClass('dd-case-study-item--margin');
      match.filter(':even').removeClass('dd-case-study-item--margin');
      match.filter('.'+value).addClass('dd-case-study-item--visible');
      match.filter('.'+value).show(400);
    });
  });
})(jQuery);
</script>
