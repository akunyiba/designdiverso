{{--
  Title: Post Listing
  Description: Post Listing
  Category: dd-blocks
  Icon: calendar
  Keywords: post, posts, grid, listing, blog, thoughts
  Mode: preview
  Align: center
  PostTypes: page post
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'dd-post-listing-' . $block['id'];
  if( !empty($block['anchor']) ) :
    $id = $block['anchor'];
  endif;

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-post-listing';

  if( !empty($block['className']) ) :
    $className .= ' ' . $block['className'];
  endif;

  if( !empty($block['align']) ) :
    $className .= ' align' . $block['align'];
  endif;

  // Load values and assing defaults.
  $filter_categories = get_categories();
@endphp

<section x-data="window.postListing()" id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
  @if( $filter_categories )
    <div class="text-center mb-10 leading-loose">
      <span class="inline-block font-grotesk text-2xl lg:text-xl">{{ __('I  want  to  learn  about ', 'sage') }}</span>
      <div @click.away="menu = false" class="relative z-50 inline-block font-grotesk text-xl">
        <a @click.prevent="menu = !menu" :class="{ 'font-bold': !menu }" class="dd-post-listing__filter-menu-link-everything" href="javascript:;">
          <span x-text="!category ? '{{ __( 'everything', 'sage' ) }}' : category"></span>
          <span :class="{ 'dd-post-listing__filter-menu-link-everything-btn--clicked': !menu }" class="dd-post-listing__filter-menu-link-everything-btn"></span>
        </a>
        <ul x-show.transition="menu" class="list-none w-48 lg:w-40 bg-white z-50 text-left absolute shadow-md px-6 py-6">
          <li><a @click.prevent="categoryAll()" :class="{ 'font-bold': category === false }" class="text-black" href="javascript:;">{{ __( 'everything', 'sage' ) }}</a></li>
          @foreach ($filter_categories as $category )
            <li><a @click.prevent="category = '{{ $category->slug }}'; restarnAnime()" :class="{ 'font-bold': category === '{{ $category->slug }}' }" class="text-black" href="javascript:;">{{ $category->name }}</a></li>
          @endforeach
        </ul>
      </div>
    </div>
    @foreach ($filter_categories as $category )
      <div x-show="category == '{{ $category->slug }}'" class="dd-post-listing__items">
          @php echo do_shortcode('[ajax_load_more loading_style="infinite ring" container_type="div" post_type="post" category="' . $category->slug  . '" transition_container="false"]'); @endphp
      </div>
    @endforeach
  @endif
  <div x-show="!category" class="dd-post-listing__items">
      @php echo do_shortcode('[ajax_load_more loading_style="infinite ring" container_type="div" post_type="post" transition_container="false"]'); @endphp
  </div>
</section>

{{--
# Repeater Template

<div class="dd-post-listing__item dd-post-listing__item--visible scene_element scene_element--fadeinup">
 <div class="dd-post-listing__item-inner">
  <a class="text-black" href="?php the_permalink(); ?>">
   <img src="?php the_post_thumbnail_url(); ?>" alt="">
   <h4 class="px-6 pt-4 pb-1 dd-anime-slide-up mb-0 text-center md:text-left">?php the_title(); ?></h4>
  </a>
 </div>
</div>

 --}}
