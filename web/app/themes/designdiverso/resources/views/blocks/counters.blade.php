{{--
  Title: Counters
  Description: Counters
  Category: dd-blocks
  Icon: calendar
  Keywords: numbers, counters
  Mode: preview
  Align: wide
  PostTypes: page post case-study
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'dd-counters-' . $block['id'];
  if( !empty($block['anchor']) ) {
      $id = $block['anchor'];
  }

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-counters';
  if( !empty($block['className']) ) {
      $className .= ' ' . $block['className'];
  }
  if( !empty($block['align']) ) {
      $className .= ' align' . $block['align'];
  }

  // Load values and assing defaults.
  $rows = get_field('counters');
@endphp

@if ($rows)
<section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
  @foreach ($rows as $item)
  @php
     $number = $item['number'];
     $text = $item['text'];
     $prefix = $item['prefix'];
     $suffix = $item['suffix'];
  @endphp
  <div class="dd-counter" aria-label="{{ $prefix . $number . $suffix }}">
      <span class="dd-counter__symbols">{{ $prefix . $number . $suffix }}</span>
      <div class="block">{{ $text }}</div>
  </div>
  @endforeach
</section>
@endif

<script>
(function($) {
  const stats = document.querySelectorAll(".dd-counter__symbols");
    stats.forEach(stat => {
      // pattern used to seperate input number from html into an array of numbers and non numbers. EX $65.3M -> ["$65.3M", "$", "65", ".", "3", "M"]
      const patt = /(\D+)?(\d+)(\D+)?(\d+)?(\D+)?/;
      const time = 200;
      let result = [...patt.exec(stat.textContent)];
      let fresh = true;
      let ticks;

      // Remove first full match from result array (we dont need the full match, just the individual match groups).
      result.shift();
      // Remove undefined values from result array where they didnt have a match in one of the optional regex groups
      result = result.filter(res => res != null);

      while (stat.firstChild) {
        stat.removeChild(stat.firstChild);
      }

      for (let res of result) {
        if (isNaN(res)) {
          stat.insertAdjacentHTML("beforeend", `<span>${res}</span>`);
        } else {
          for (let i = 0; i < res.length; i++) {
            stat.insertAdjacentHTML(
              "beforeend",
              `<span data-value="${res[i]}">
                <span>&ndash;</span>
                ${Array(parseInt(res[i]) + 1)
                  .join(0)
                  .split(0)
                  .map(
                    (x, j) => `
                  <span>${j}</span>
                `
                  )
                  .join("")}
              </span>`
            );
          }
        }
      }

      ticks = [...stat.querySelectorAll("span[data-value]")];

      let activate = () => {
        let top = stat.getBoundingClientRect().top;
        let offset = window.innerHeight * 3 / 4;

        setTimeout(() => {
          fresh = false;
        }, time);

        if (top < offset) {
          setTimeout(() => {
            for (let tick of ticks) {
              let dist = parseInt(tick.getAttribute("data-value")) + 1;
              tick.style.transform = `translateY(-${dist * 100}%)`;
            }
          }, fresh ? time : 0);
          window.removeEventListener("scroll", activate);
        }
      };
      window.addEventListener("scroll", activate);
      activate();
    });
})(jQuery);
</script>
