{{--
  Title: Latest Posts Tweet
  Description: Latest Posts
  Category: dd-blocks
  Icon: grid-view
  Keywords: posts, blog
  Mode: preview
  Align: wide
  PostTypes: page post
  SupportsAlign: false
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'dd-latest-posts-tweet-' . $block['id'];
  if( !empty($block['anchor']) ) {
      $id = $block['anchor'];
  }

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-latest-posts-tweet';
  if( !empty($block['className']) ) {
      $className .= ' ' . $block['className'];
  }
  if( !empty($block['align']) ) {
      $className .= ' align' . $block['align'];
  }

  // Load values and assing defaults.
  $args = array(
          'post_type' => 'post',
          'posts_per_page' => 3,
          'no_found_rows' => true
      );

  $post_query = new WP_Query($args);
  $posts = $post_query->get_posts();
@endphp

<section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
  @foreach ($posts as $post)
    @php
      $title = $post->post_title;
      $author = get_the_author_meta('first_name', $post->post_author) . ' ' . get_the_author_meta('last_name', $post->post_author);
      $author_prefix = __('Written By ', 'sage');
      $read_more_link = get_the_permalink($post->ID);
      $read_more_text = __('Read More', 'sage');
      $image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
      if ( $image_alt = get_the_post_thumbnail_caption( $post->ID ) ):
        // Nothing to do here
      else:
        $image_alt = get_the_title( $post->ID );
      endif;
    @endphp
    <div class="dd-latest-post">
      <div class="w-full h-full relative overflow-hidden">
      <img src="{{ esc_url($image_url) }}" alt="{{ esc_attr($image_alt) }}">
        <div class="dd-latest-post__overlay">
          <div class="dd-latest-post__content">
            <h3 class="md:text-white">{{  $title }}</h3>
            <p class="text-placeholder">{{ $author_prefix . $author }}</p>
            <a class="mt-2" href="{{ $read_more_link }}">{{ $read_more_text }}</a>
          </div>
        </div>
      </div>
    </div>
  @endforeach
      <div class="dd-latest-tweet">
        @php echo do_shortcode( '[recent_tweets id="designdiverso" dom_id="dd-twitter-feed" max_tweets="1" enable_links="true" show_user="true" show_time="true" show_retweet="false" show_interaction="false" show_images="true" link_is_in_new_window="true" lang="en" show_permalinks="true"]' ) @endphp
        {{-- <a class="twitter-timeline" data-tweet-limit="1" data-height="202" data-chrome="noheader, nofooter, noborders, transparent, noscrollbar" href="https://twitter.com/designdiverso?ref_src=twsrc%5Etfw">Tweets by designdiverso</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> --}}
      </div>
</section>
