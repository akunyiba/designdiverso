{{--
  Title: Plans Column
  Description: Plans Column
  Category: dd-blocks
  Icon: editor-table
  Keywords: plan, plans, column, columns
  Mode: preview
  Align: center
  PostTypes: page post
  SupportsAlign: left right center
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'dd-plans-column-' . $block['id'];
  if( !empty($block['anchor']) ) {
      $id = $block['anchor'];
  }

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-plans-column scene_element scene_element--fadeinright';
  if( !empty($block['className']) ) {
      $className .= ' ' . $block['className'];
  }
  if( !empty($block['align']) ) {
      $className .= ' align' . $block['align'];
  }

  // Load values and assing defaults.
  $bg_color = get_field('background_color');
  $title = get_field('title');
  $options = get_field('options');
  $price = get_field('price');
  $plan_id = get_field('plan_id');
  $button = get_field('button');
@endphp

<div id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}" style="background-color: {{ esc_attr( $bg_color ) }};">
  <h3 class="text-center mb-8">{{ $title }}</h3>
  @foreach ($options as $option)
    @if ( $button['page_link'] )
      @php $button_link = $button['page_link'] @endphp
    @elseif ( $button['link'] )
      @php $button_link = $button['link'] @endphp
    @endif
    <div class="dd-plans-column-accordion__item">
      <a href="javascript:;" class="text-white"><h4 class="flex justify-between"><span>{{ $option['title'] }}</span></h3></a>
      <div class="dd-plans-column-accordion__item__text hidden">
        <p class="text-white">{{ $option['text'] }}</p>
      </div>
    </div>
  @endforeach
  <div class="p-10">
    <p class="text-center block my-10">{{ $price }}</p>
    <a data-plan-id="{{ esc_attr($plan_id) }}" href="{{ esc_url($button_link) }}" class="text-center block dd-btn dd-btn--outline-white hover:bg-black hover:border-black">{{ $button['text'] }}</a>
  </div>
</div>
