{{--
  Title: Hero
  Description: Hero section
  Category: dd-blocks
  Icon: slides
  Keywords: hero
  Mode: preview
  Align: wide
  PostTypes: page post
  SupportsAlign: left center right wide full
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'dd-hero-' . $block['id'];
  if( !empty($block['anchor']) ) {
      $id = $block['anchor'];
  }

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-hero';
  if( !empty($block['className']) ) {
      $className .= ' ' . $block['className'];
  }
  if( !empty($block['align']) ) {
      $className .= ' align' . $block['align'];
  }

  // Load values and assing defaults.
  $content = get_field('content');
  $heading = get_field('heading');
  $buttons = get_field('buttons');
  $scroll = get_field('scroll');
  $bg_color = get_field('background_color');

  if( !$bg_color ){
    $bg_image = get_field('background_image') ?: 11;
  }
  else {
    $className .= ' dd-hero--bg-color';
    $bg_image = '';
  }

  if( $scroll['target'] ) {
    $className .= ' dd-hero--custom-scroll';
  }
@endphp

<section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
  <div class="dd-hero__inner">
    <div class="dd-hero__content">
        @if( $heading )
          <h1 class="font-grotesk font-medium text-white text-h1-md md:text-h1-hero leading-08 md:leading-none tracking-tight ml3">{!! $heading !!}</h1>
        @endif

        @if( $content )
          <div class="mt-10 text-white md:block">{!! $content !!}</div>
        @endif

        @if( $buttons )
          @foreach ($buttons as $item)
            @php $button_classes = '' @endphp
            @if ($loop->iteration == 1)
              @php $button_classes = 'inline-block dd-btn dd-btn--outline-white mt-8 text-white text-center' @endphp
            @else
              @php $button_classes = 'inline-block dd-btn dd-btn--primary mt-8 text-white text-center hover:border-black' @endphp
            @endif
            @foreach ($item as $button)
              @php $button['text'] ? $button_text = $button['text'] : $button_text = __('Click me', 'sage'); @endphp
              @if ( $button['file_link'] )
                @php $button_link = $button['file_link'] @endphp
              @elseif ( $button['page_link'] )
                @php $button_link = $button['page_link'] @endphp
              @elseif ( $button['link'] )
                @php $button_link = $button['link'] @endphp
              @else
                @php $button_link = '#' @endphp
              @endif
                <a class="{{ $button_classes }}" href="{{ $button_link }}">{{ $button_text }}</a>
            @endforeach
          @endforeach
        @endif
    </div>
  </div>
  @if( $scroll['target'] )
    <div class="hidden md:flex justify-center h-24">
      <a class="border-none" href="{{ '#' . $scroll['target'] }}">
        <span class="dd-text-hidden">{{ __('Scroll down', 'sage') }}</span>
        <svg class="h-full" version="1.1" id="scroll-icon" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 328 386" xml:space="preserve" width="54" height="64">
          <path class="dd-scroll-path" d="M161.5 83.5h0c32 0 58 26 58 58v69.9c0 32-26 58-58 58h0c-32 0-58-26-58-58v-69.9c0-32 26-58 58-58z" fill="none" stroke="#000" stroke-width="2.37" stroke-miterlimit="10"/><circle class="circle" cx="162.9" cy="137.3" r="21.6" fill="none" stroke="#000" stroke-width="1.8" stroke-miterlimit="10"/><path class="st2" d="M144 291.7l20.7 20.8M184.5 291.7l-20.8 20.8"/><path fill="none" d="M0 0h328v386H0z"/></svg>
      </a>
    </div>
  @endif
</section>

<style type="text/css">
  [data-{{ esc_attr($id) }}] .dd-hero__inner {
    background-image: url({{ wp_get_attachment_image_url($bg_image, 'full') }});
    background-color: {{ $bg_color }};
  }

  /* SVG */
  .st2 {
    fill:none;
    stroke:#000;
    stroke-width:2.1;
    stroke-miterlimit:10;
  }

  .dd-scroll-path, .st2, .circle {
    animation: sdb04 1.5s ease-out infinite;
  }
  /* Success page TODO: */
  .page-id-2959 .dd-scroll-path, .page-id-2959 .st2, .page-id-2959 .circle {
    stroke-width: 10px;
  }

  @keyframes sdb04 {
    0% {
      transform: translate(0, -20px);
    }
    20% {
      transform: translate(0, -10px);
    }
    40% {
      transform: translate(0, 0);
    }
    50% {
      transform: translate(0, 10px);
    }
    60% {
      transform: translate(0, 0);
    }
    80% {
      transform: translate(0, -10px);
    }
    100% {
      transform: translate(0, -20px);
    }
  }
</style>

<script>
  var textWrapper = document.querySelector('.ml3');
  textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, '<span class="letter">$&</span>');
</script>
