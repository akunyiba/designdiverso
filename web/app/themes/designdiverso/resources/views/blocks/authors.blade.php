{{--
  Title: Authors
  Description: Authors
  Category: dd-blocks
  Icon: list-view
  Keywords: author, authors, list
  Mode: preview
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: false
  SupportsMultiple: true
--}}

@php
  // Create id attribute allowing for custom "anchor" value.
  $id = 'dd-authors-' . $block['id'];
  if( !empty($block['anchor']) ) {
      $id = $block['anchor'];
  }

  // Create class attribute allowing for custom "className" and "align" values.
  $className = 'dd-authors';
  if( !empty($block['className']) ) {
      $className .= ' ' . $block['className'];
  }
  if( !empty($block['align']) ) {
      $className .= ' align' . $block['align'];
  }

  // Load values and assing defaults.
	$users = get_users();
@endphp

@if($users)
  <section id="{{ esc_attr($id) }}" data-{{ esc_attr($id) }} class="{{ esc_attr($className) }}">
    @foreach ($users as $user)
    @php
      $data = $user->data;
      $user_id = (int)$data->ID;
      $user_meta = get_userdata($user_id);
      $roles = $user_meta->roles;
      $posts = (int)count_user_posts($user_id);
      $allowed_roles = array('editor', 'administrator', 'author');
    @endphp
    @if (array_intersect($allowed_roles, $roles ) && $posts > 0 )
      @php
        // Setup vars
        $name = get_the_author_meta('nickname', $user_id);
        $avatar = get_wp_user_avatar_src($user_id);
        $url = ($posts > 0 ? get_author_posts_url($user_id) : '');
        $description = get_the_author_meta('description', $user_id);
      @endphp
      <div class="dd-author flex border-grey pt-8 mb-8">
        <div class="pr-6">
          @include('partials.user-avatar', array('id' => $user_id))
        </div>
        <div>
          <h3><a class="text-black" href="{{ get_author_posts_url( get_the_author_meta( 'ID', $user_id ) ) }}">{{ get_the_author_meta('display_name', $user_id) }}</a></h3>
          <p class="text-grey-dark">{{ $description }}</p>
        </div>
      </div>
    @endif
  @endforeach
  </section>
@endif
