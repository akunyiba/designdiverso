@php
  // TODO: Theme settings
  $terms_of_service_url = wp_get_attachment_url(3141);
  $privacy_policy_url = wp_get_attachment_url(3140);
@endphp

<section class="scene_element scene_element--fadeinright">
    <form action="/app/chargebee/StripeJs3dsCheckout.php" method="post" id="subscribe-form" class="dd-checkout-form">
      <h3 class="is-size-4" style="margin-bottom: 3rem ;">1. Tell us about yourself</h3>
      <div class="columns flex flex-col xl:flex-row">
          <div class="column w-full xl:w-1/2 xl:pr-2">
              <div class="flex flex-col md:flex-row">
                  <div class="control is-expanded w-full md:w-1/2" id="first-name">
                      <input type="text" placeholder="First Name*" class="input" name="customer[first_name]"
                              maxlength=50 required data-msg-required="Please enter your first name">
                      <small for="customer[first_name]" class="has-text-danger"></small>
                      <i class="fas fa-check"></i>
                  </div>
                  <div class="control is-expanded w-full md:w-1/2" id="last-name">
                      <input type="text" placeholder="Last Name*" class="input" name="customer[last_name]"
                              maxlength=50 required data-msg-required="Please enter your last name">
                      <small for="customer[last_name]" class="has-text-danger"></small>
                      <i class="fas fa-check"></i>
                  </div>
              </div>

              <div class="flex flex-col md:flex-row">
                  <div class="control is-expanded w-full md:w-1/2" id="e-mail">
                      <input id="email" type="email" placeholder="Email*" class="input" name="customer[email]" maxlength=50
                              data-rule-required="true" data-rule-email="true"
                              data-msg-required="Please enter your email address"
                              data-msg-email="Please enter a valid email address">
                      <small for="customer[email]" class="has-text-danger"></small>
                      <i class="fas fa-check"></i>
                  </div>
                  <div class="control is-expanded w-full md:w-1/2" id="phone-number">
                      <input id="phone" type="text" placeholder="Phone*" class="input" name="customer[phone]" maxlength=20 required data-msg-required="Please enter your phone number" data-rule-phonenu="true" data-msg-phonenu="please enter an international phone number in a similar format : +442071838750">
                      <small for="customer[phone]" class="has-text-danger"></small>
                      <i class="fas fa-check"></i>
                  </div>
              </div>
          </div>
          <div class="column w-full xl:w-1/2 xl:pl-2">
              <div class="flex">
                  <div class="control w-full" id="address">
                      <input type="text" class="input" placeholder="Billing address*" name="addr" maxlength=50 required data-msg-required="Please enter your billing address">
                      <small for="addr" class="has-text-danger"></small>
                      <i class="fas fa-check"></i>
                  </div>
              </div>
              <div class="field is-grouped is-grouped-multiline-mobile" style="max-height: none;">
                  <div class="flex flex-col md:flex-row">
                      <div class="is-inline-block-tablet" id="city">
                          <input type="text" class="input" placeholder="City*" name="city" maxlength=50 required data-msg-required="Please enter your city">
                          <small for="city" class="has-text-danger"></small>
                          <i class="fas fa-check"></i>
                      </div>
                      <div class="is-inline-block-tablet" id="state">
                          <input type="text" class="input" placeholder="State / Region*" name="state" maxlength=20 required data-msg-required="Please enter your state">
                          <small for="state" class="has-text-danger"></small>
                          <i class="fas fa-check"></i>
                      </div>
                      <div class="is-inline-block-tablet" id="zip-code">
                          <input id="zip_code" placeholder="Zip / Postal code*" type="text" class="input" name="zip_code" maxlength=10 required number data-msg-required="Please enter your zip / postal code">
                          <small for="zip_code" class="has-text-danger"></small>
                          <i class="fas fa-check"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <h3 class="is-size-4" id="payment_header">2. Payment Information</h3>
          <div class="stripe-card-field_wrapper field">
              <label for="card-element" class="label mb-4 block">Credit or debit card</label>
              <div id="card-element" class="stripe-card-field"></div>
              <img class="w-48 mt-2" src="@asset('images/trust-badges.svg')" alt="__('Trust badges', 'sage')">
          </div>

      <hr style="margin-top: 4rem;">
      <p>By clicking Subscribe, you agree to our <a href="{{ $privacy_policy_url }}" target="_blank">privacy policy</a> and <a target="_blank" href="{{ $terms_of_service_url  }}">terms of service</a>.</p>
      <p>
          <small class="has-text-danger" style="display:none;">There were errors while submitting</small>
      </p>

      <div class="field">
          <div class="control" id="loading-form" style="max-height: none ;">
              <input type="submit" class="dd-btn dd-btn--primary mt-6 submit-button" value="Subscribe" id="subscribe-btn">
              <small class="alert-danger has-text-danger"></small>
          </div>
      </div>
    </form>
  <div class="flex flex-col md:flex-row mt-20">
      <div class="w-full md:w-1/2 md:pr-6">
          <h4 class="is-size-4" style="margin-bottom: 1rem ;">We value your Privacy.</h4>
      <p>We will not sell or distribute your contact information. Read our <a target="_blank" href="{{$privacy_policy_url}}">Privacy Policy</a>.</p>
      </div>
      <div class="mt-10 md:mt-0 w-full md:w-1/2 md:pl-6">
          <h4 class="is-size-4" style="margin-bottom: 1rem ;">Billing Enquiries</h4>
          <p>Do not hesitate to reach our <a target="_blank" href="mailto:support@designdiverso.com">support team</a> if you have any queries.</p>
      </div>
  </div>
</section>

<script>
  (function($) {

    jQuery(document).ready(function(){
      var plan_id = localStorage.getItem('plan_id');
    var form = $('#subscribe-form');

    if ( plan_id === undefined || plan_id === null ) plan_id = 'pro';

    form.append('<input type="hidden" name="plan" value="' + plan_id + '" />');
          if($.validator !== undefined || $.validator != null) {
            $.validator.methods.email = function( value, element ) {
              var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return this.optional( element ) || re.test( value );
            } ;

            $.validator.addMethod('phonenu', function(value, element) {
              var re = /^\+((?:9[679]|8[035789]|6[789]|5[90]|42|3[578]|2[1-689])|9[0-58]|8[1246]|6[0-6]|5[1-8]|4[013-9]|3[0-469]|2[70]|7|1)(?:\W*\d){0,13}\d$/;
              return this.optional( element ) || re.test( value );
            }) ;
          }

          var firstNameField = $('#first-name input') ;
          var firstNameId = '#first-name';
          firstNameField.on('keyup', function () {
            validateOnUp(firstNameField, firstNameId) ;
          }) ;

          var lastNameField = $('#last-name input') ;
          var lastNameId = '#last-name' ;
          lastNameField.on('keyup', function () {
            validateOnUp(lastNameField, lastNameId) ;
          }) ;

          var emailField = $('#e-mail input') ;
          var emailId = '#e-mail' ;
          // emailField.focusout(function () {
          //   var emailControl = $('#e-mail') ;
          //   $.ajax({
          //       type: 'POST',
          //       url: '/app/chargebee/CheckEmail.php',
          //       beforeSend: function() {
          //         emailControl.addClass('is-loading') ;
          //         $(emailId + ' .fas').hide() ;
          //         $('.submit-button').attr('disabled', 'disabled');
          //         $('#loading-form').addClass('is-loading') ;
          //       },
          //       data: {email: emailField.val()},
          //       success: function(data){
          //         emailControl.removeClass('is-loading') ;
          //         $('#loading-form').removeClass('is-loading') ;
          //         if(data == 'true') {
          //           emailField.removeClass('has-text-danger') ;
          //           emailField.addClass('dd-input-sucess') ;
          //           $(emailId + ' .fas').fadeIn('slow') ;
          //           $('.submit-button').removeAttr('disabled');
          //         }
          //         else {
          //           emailField.addClass('has-text-danger') ;
          //           emailField.removeClass('dd-input-sucess') ;
          //           $(emailId + ' .fas').hide() ;
          //           $('.submit-button').attr('disabled', 'disabled');
          //         }
          //       },
          //   });
          // }) ;

          var phoneField = $('#phone-number input') ;
          var phoneId = '#phone-number' ;
          phoneField.on('keyup', function () {
            validateOnUp(phoneField, phoneId) ;
          }) ;

          var cityField = $('#city input') ;
          var cityId = '#city' ;
          cityField.on('keyup', function () {
            validateOnUp(cityField, cityId) ;
          }) ;

          var stateField = $('#state input') ;
          var stateId = '#state' ;
          stateField.on('keyup', function () {
            validateOnUp(stateField, stateId) ;
          }) ;

          var zipCodeField = $('#zip-code input') ;
          var zipCodeId = '#zip-code' ;
          zipCodeField.on('keyup', function () {
            validateOnUp(zipCodeField, zipCodeId) ;
          }) ;

          var addressField = $('#address input') ;
          var addressId = '#address' ;
          addressField.on('keyup', function () {
            validateOnUp(addressField, addressId) ;
          }) ;

          $('#subscribe-form').validate({
            rules: {
                zip_code: {
                  number: true,
                },
                phone: {
                  number: true,
                  required: true,
                  phonenu: true,
                },
            },
          });

          function formValidationCheck(form) {
              // Checking form has passed the validation.
              if (!$(form).valid()) {
                  return false;
              }
              $('.alert-danger').hide();
              $('#loading-form').addClass('is-loading') ;
              $('#loading-form').attr('value', '') ;
          }

          $('#subscribe-form').on('submit', function() {
              // form validation
              formValidationCheck(this);
              if(!$(this).valid()){
                  return false;
              }
              // Disable the submit button to prevent repeated clicks and form submit
              $('.submit-button').attr('disabled', 'disabled');

              return false; // submit from callback
          });

          // Setting the error class and error element for form validation.
          if (jQuery.validator !== undefined || jQuery.validator != null) {
            jQuery.validator.setDefaults({
              errorClass: 'has-text-danger',
              errorElement: 'small',
            });
          }

                var stripe = Stripe('pk_live_y8I59xPrpThJstEZprQJImRq');
                // var stripe = Stripe('pk_test_DuEehYmBCISZILtl04RiTLnB');
                var c_style = {
                  base: {
                    color: '#363636',
                    fontSmoothing: 'antialiased',
                    fontSize: '16px',
                  },
                }

                var elements = stripe.elements();
                var cardElement = elements.create('card', {style: c_style});
                cardElement.mount('#card-element');

                var cardButton = document.getElementById('subscribe-btn');

                cardButton.addEventListener('click', function (ev) {
                    ev.preventDefault();

                    stripe.createPaymentMethod('card', cardElement, {})

                    .then(function (result) {
                        if (result.error) {
                            // Show error in payment form
                        } else {
                            // console.log(result.paymentMethod.id);
                            // debugger;

                            //Otherwise send paymentMethod.id to your server
                            fetch('/app/chargebee/StripeJs3dsCheckout.php?confirm_payment', {
                                method: 'POST',
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify({
                                    payment_method_id: result.paymentMethod.id,
                                    addr: $('[name="addr"]').val(),
                                    // extended_addr: $('[name="extended_addr"]').val(),
                                    city: $('[name="city"]').val(),
                                    state: $('[name="state"]').val(),
                                    zip_code: $('[name="zip_code"]').val(),
                                    plan: $('[name="plan"]').val(),
                                }),
                            })
                            .then(function (result) {
                                // Handle server response
                                result.json().then(function (json) {
                                    handleServerResponse(json);
                                })
                            });
                            // .then(resp => resp.text()).then(console.log);
                        }
                    });
                });

                function handleServerResponse(response) {
                    if (response.error) {
                        // Show error from server on payment form
                    } else if (response.requires_action) {
                        // Use Stripe.js to handle required card action
                        handleAction(response);
                    } else {
                        console.log('payment_intent_id: ', response.payment_intent_id, 'success: ', response.success);
                        // debugger;

                        // Show success message and call create subscription
                        fetch('/app/chargebee/StripeJs3dsCheckout.php?checkout', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                customer: {
                                    first_name: $('[name="customer[first_name]"]').val(),
                                    last_name: $('[name="customer[last_name]"]').val(),
                                    email: $('[name="customer[email]"]').val(),
                                    phone: $('[name="customer[phone]"]').val(),
                                },
                                addr: $('[name="addr"]').val(),
                                // extended_addr: $('[name="extended_addr"]').val(),
                                city: $('[name="city"]').val(),
                                state: $('[name="state"]').val(),
                                zip_code: $('[name="zip_code"]').val(),
                                plan: $('[name="plan"]').val(),
                                payment_intent_id: response.payment_intent_id,
                            }),
                        }).then(response => response.json()).then(function (responseJSON) {
                            // debugger;
                            window.location.replace(responseJSON.forward);
                            // console.log(responseJSON);
                        });
                    }
                }

                function handleAction(response) {
                    stripe.handleCardAction(
                        response.payment_intent_client_secret
                    ).then(function (result) {
                        if (result.error) {
                            // Show error in payment form
                            // console.log(JSON.stringify(result));
                            $('.alert-danger').show().text(result.error.message);
                            // debugger;
                        } else {
                            // The card action has been handled
                            // The PaymentIntent can be confirmed again on the server
                            fetch('/app/chargebee/StripeJs3dsCheckout.php?confirm_payment', {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                },
                                body: JSON.stringify({
                                    payment_intent_id: result.paymentIntent.id,
                                }),
                            }).then(response => response.json()).then(function (confirmResult) {
                                // console.log('payment_intent_id: ', confirmResult.payment_intent_id, 'success: ', confirmResult.success);
                                handleServerResponse(confirmResult);
                            });
                            // debugger;
                        }
                    });
                }

    function validateOnUp($field, $id) {
      if($field.valid()){
        $field.addClass('dd-input-sucess') ;
        $($id + ' .fas').fadeIn('slow') ;
      }
      else {
        $field.removeClass('dd-input-sucess') ;
        $($id + ' .fas').hide() ;
      }
    }
    });
})(jQuery);
</script>
