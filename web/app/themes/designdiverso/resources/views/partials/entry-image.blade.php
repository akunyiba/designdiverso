@php
  // TODO: Check where this can be reused site-wide
  $image_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
  if ( $image_alt = get_the_post_thumbnail_caption(get_the_ID())):
    // Nothing to do here
  else:
    $image_alt = get_the_title(get_the_ID());
  endif;
@endphp

<div class="entry-image my-6">
  <img class="w-full" src="{{ esc_url($image_url) }}" alt="{{ esc_attr($image_alt) }}">
</div>
