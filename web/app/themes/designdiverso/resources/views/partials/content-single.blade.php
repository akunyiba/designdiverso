@php
  $newsletter_gutenberg_reusable_block = get_post( 249 );
@endphp

<article @php post_class() @endphp>
  <header class="md:px-20">
    @php
      // TODO: Make partial
      $categories = array_reverse(get_the_category());
      $category = array_pop($categories);
      $blog_link = get_page_link('214');
    @endphp
    <span class="cl-effect-4"><a class="dd-entry-header__category-link" href="{{ esc_url( $blog_link ) }}">{{ 'Thoughts' }}</a></span>
    <span class="text-primary">{{ '//' }}</span>

    <span
      class="dd-entry-header__category-link">{{ $category->name }}</span>

    <div class="flex justify-between flex-col lg:flex-row mb-10 lg:mb-0">
        <h1 class="entry-title capitalize mt-4 mb-4 font-grotesk font-extrabold text-h1 pr-6">{!! get_the_title() !!}</h1>
        <a href="#subscribe" class="hover:border-transparent"><button class="dd-btn dd-btn--outline mt-0">{{ __( 'Subscribe', 'sage' ) }}</button></a>
    </div>

    @include('partials/entry-meta')

    @php // TODO: Make partial
      $image_url = get_the_post_thumbnail_url( get_the_ID(), 'full' );

      if ( $image_alt = get_the_post_thumbnail_caption() ):
        // Nothing to do here
      else:
      $image_alt = get_the_title();
      endif;
    @endphp
    <img src="{{ esc_url( $image_url ) }}" alt="{{ esc_attr($image_alt) }}" class="w-full mt-6 mb-6 md:mb-32 md:pr-10 object-cover h-48 md:h-auto">

  </header>

  <div class="entry-content">
    @if (shortcode_exists('shared_counts'))
      @php echo do_shortcode('[shared_counts]') @endphp
    @endif
    @php the_content() @endphp
  </div>

  <div id="subscribe" class="mt-32">
    @php
    // get reusable gutenberg block:
    echo apply_filters( 'the_content', $newsletter_gutenberg_reusable_block->post_content );
    @endphp
  </div>

  <div class="alignwide mt-16">
    @include('partials.recent-posts')
  </div>

  <div id="disqus_thread" class="md:px-32 mt-32"></div>

<script>
@php
function dsq_identifier_for_posts($post)
{
    return get_the_ID() . ' ' . get_the_guid();
}
@endphp
var disqus_config = function () {
this.page.url = '<?php echo get_permalink(); ?>'; // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = '<?php echo dsq_identifier_for_posts($post) ?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function() { // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
var d = document, s = d.createElement('script');

s.src = '//designdiverso.disqus.com/embed.js'; // IMPORTANT: Replace EXAMPLE with your forum shortname!

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
</article>
