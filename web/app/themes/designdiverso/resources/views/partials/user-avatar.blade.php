@php
  isset( $id ) ? '' : $id = get_the_ID();
@endphp

<div class="user-avatar">
  {{-- <img src="{{ get_avatar_url( $id ) }}" alt="{{ get_the_author_meta( 'first_name', $id ) . ' ' . get_the_author_meta( 'last_name', $id ) }}"> --}}
  @php echo get_avatar($id) @endphp
</div>
