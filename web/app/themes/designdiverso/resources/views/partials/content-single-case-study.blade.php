<article @php post_class() @endphp>
    <header>
      <div class="flex justify-end flex-col lg:flex-row mt-10 lg:mt-0">
          <h1 class="entry-title capitalize mt-4 mb-4 font-grotesk font-extrabold text-h1">{!! get_the_title() !!}</h1>
      </div>
    </header>

    <div class="entry-content">
      @php the_content() @endphp
    </div>

    <div class="alignfull mt-32 mb-24">
      @include('partials.recent-posts', array('type' => 'case-study'))
    </div>

    @php
      // "Let’s work together" reusable block
      if( function_exists('reblex_display_block')) {
        reblex_display_block(435);
      }
    @endphp
  </article>
