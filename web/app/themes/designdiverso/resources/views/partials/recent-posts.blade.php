@php
  isset( $type ) ? '' : $type = 'post';
  $args = array(
    'post_type' => $type,
    'posts_per_page' => 9,
    'no_found_rows' => true,
    'post__not_in' => array( get_the_ID() )
  );
  $post_query = new WP_Query($args);
  $posts = $post_query->get_posts();
@endphp

<section class="recent-posts glide">
  <div class="glide__track" data-glide-el="track">
    <ul class="glide__slides">
      @foreach ($posts as $post)
        @php
          $title = $post->post_title;
          $read_more_link = get_the_permalink($post->ID);
          $image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
          if ( !$image_alt = get_the_post_thumbnail_caption( $post->ID ) ) {
            $image_alt = get_the_title( $post->ID );
          }
        @endphp
        <div class="recent-post glide__slide">
          <a href="{{ esc_url($read_more_link) }}" class="text-black border-none">
            <div class="img-hover-zoom img-hover-zoom--colorize h-48 md:h-64">
              <img src="{{ esc_url($image_url) }}" alt="{{ esc_attr($image_alt) }}" class="w-full h-full object-cover">
            </div>
            <h3 class="mt-2 pl-6">{{ $title }}</h3>
          </a>
        </div>
      @endforeach
    </ul>
  </div>
  <div class="glide__arrows" data-glide-el="controls">
    {{-- TODO: update Tailwind --}}
    <button class="glide__arrow glide__arrow--left shadow-none w-10 h-10 p-0 border-0 text-white hover:text-primary transition ease-in duration-300" data-glide-dir="<"><svg style="transform: rotate(180deg);" class="w-full fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.008 512.008"><path d="M381.048 248.633L146.381 3.299C143.36.153 138.735-.868 134.693.778a10.655 10.655 0 00-6.688 9.896v42.667c0 2.729 1.042 5.354 2.917 7.333l185.063 195.333-185.062 195.334a10.659 10.659 0 00-2.917 7.333v42.667c0 4.354 2.646 8.281 6.688 9.896a10.64 10.64 0 003.979.771c2.854 0 5.646-1.146 7.708-3.292l234.667-245.333c3.938-4.125 3.938-10.625 0-14.75z"/></svg></button>
    <button class="glide__arrow glide__arrow--right shadow-none w-10 h-10 p-0 border-0 text-white hover:text-primary transition ease-in duration-300" data-glide-dir=">"><svg class="w-full fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.008 512.008"><path d="M381.048 248.633L146.381 3.299C143.36.153 138.735-.868 134.693.778a10.655 10.655 0 00-6.688 9.896v42.667c0 2.729 1.042 5.354 2.917 7.333l185.063 195.333-185.062 195.334a10.659 10.659 0 00-2.917 7.333v42.667c0 4.354 2.646 8.281 6.688 9.896a10.64 10.64 0 003.979.771c2.854 0 5.646-1.146 7.708-3.292l234.667-245.333c3.938-4.125 3.938-10.625 0-14.75z"/></svg></button>
  </div>
</section>
