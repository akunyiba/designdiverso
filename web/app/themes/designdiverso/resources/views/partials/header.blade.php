
@php
  get_field('logo') ? $logo = get_field('logo') : $logo = '';
  get_field('navigation') ? $nav_off = get_field('navigation') : $nav_off = '';
  $header_bg = get_field('header_background');
@endphp
@if($header_bg)
  @php
    $header_bg['header_background_color'] ? $header_bg_color = $header_bg['header_background_color'] : $header_bg_color  = '';
    $header_bg['header_background_transparency'] ? $header_bg_transparency = $header_bg['header_background_transparency'] : $header_bg_transparency = '';
  @endphp
@endif
<header class="banner flex">
  <div class="banner__inner wrap">
      <div class="lg:px-24 lg-alignfull flex justify-between items-center">
          @if ( function_exists( 'the_custom_logo' ) )
            @if($logo == 'white')
              {{-- TODO: Theme settings  --}}
              @php $custom_logo_id = 3185; @endphp
            @else
              @php $custom_logo_id = get_theme_mod( 'custom_logo' ); @endphp
            @endif
            @php
              $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
              $alt = get_bloginfo();
            @endphp
            <a class="dd-logo z-40 border-none flex" href="{{ home_url('/') }}">
              <img class="w-48 md:w-64" src="{{ esc_url( $custom_logo_url )}}" alt="{{ $alt }}">
            </a>
            @else
              <a class="dd-logo border-none" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
            @endif
            @if(!$nav_off)
              <a href="javascript:;" class="dd-hamburger">
                <span class="dd-text-hidden">{{ __('Menu', 'sage')}}</span>
                <span class="dd-hamburger__lines"></span>
              </a>
            <nav class="nav-primary">
              @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
              @endif
            </nav>
            <nav class="dd-nav-secondary">
              <a href="mailto:tancredi@designdiverso.com"><span class="dd-text-hidden">{{ __('Email', 'sage')}}</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M469.333 64H42.667C19.135 64 0 83.135 0 106.667v298.667C0 428.865 19.135 448 42.667 448h426.667C492.865 448 512 428.865 512 405.333V106.667C512 83.135 492.865 64 469.333 64zM42.667 85.333h426.667c1.572 0 2.957.573 4.432.897-36.939 33.807-159.423 145.859-202.286 184.478-3.354 3.021-8.76 6.625-15.479 6.625s-12.125-3.604-15.49-6.635C197.652 232.085 75.161 120.027 38.228 86.232c1.478-.324 2.866-.899 4.439-.899zm-21.334 320V106.667c0-2.09.63-3.986 1.194-5.896 28.272 25.876 113.736 104.06 169.152 154.453C136.443 302.671 50.957 383.719 22.46 410.893c-.503-1.814-1.127-3.588-1.127-5.56zm448 21.334H42.667c-1.704 0-3.219-.594-4.81-.974 29.447-28.072 115.477-109.586 169.742-156.009a7980.773 7980.773 0 0018.63 16.858c8.792 7.938 19.083 12.125 29.771 12.125s20.979-4.188 29.76-12.115a8178.815 8178.815 0 0018.641-16.868c54.268 46.418 140.286 127.926 169.742 156.009-1.591.38-3.104.974-4.81.974zm21.334-21.334c0 1.971-.624 3.746-1.126 5.56-28.508-27.188-113.984-108.227-169.219-155.668 55.418-50.393 140.869-128.57 169.151-154.456.564 1.91 1.194 3.807 1.194 5.897v298.667z"/></svg></a>
            </nav>
          @endif
      </div>
  </div>
</header>
