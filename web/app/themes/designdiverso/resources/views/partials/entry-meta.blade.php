<div class="flex">
      @include('partials.user-avatar', array('id' => get_the_author_meta( 'ID' )))
      <div class="ml-4">
          <p class="byline author vcard mr-2">
              {{ __( 'Written by', 'sage') }} <span class="cl-effect-4"><a href="{{ get_author_posts_url( get_the_author_meta( 'ID' ) ) }}" rel="author" class="fn">{{ get_the_author_meta( 'first_name' ) . ' ' . get_the_author_meta( 'last_name' ) }}</a></span>
              {{ __('in', 'sage') }}
              @php
                  $filter_categories = get_the_category();
                  $filter_categories_reverse = array_reverse($filter_categories);
                  $category = array_pop( $filter_categories_reverse );
                  $category_link = get_category_link( $filter_categories[0]->term_id );
                @endphp
                 <span class="cl-effect-4"><a
                 class="dd-entry-meta__category-link"
                 href="{{ $category_link }}">{{ $category->name }}</a><span class="cl-effect-4">
            </p>
            <time class="text-xl" datetime="{{ get_post_time('c', true) }}">{{ __( 'on', 'sage' ) . ' ' . get_the_date() }}</time>
          <p class="inline-block">/ <span>{{ App\dd_reading_time(get_the_ID()) }}</span> <span>{{ __('Read time', 'sage') }}</span></p>
      </div>
</div>
