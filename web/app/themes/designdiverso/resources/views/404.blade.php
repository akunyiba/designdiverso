@extends('layouts.app')

@section('content')
  @php
    $post = get_post(2858); //TODO: Theme settings
    $the_content = apply_filters('the_content', $post->post_content);
  @endphp
  @if ( !empty($the_content) )
    {!! $the_content !!}
  @endif
@endsection
