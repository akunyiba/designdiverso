@extends('layouts.app')

@section('content')

  <div class="flex border-b border-b border-grey mb-24 pb-10">
    <div class="pr-6">
      @include('partials.user-avatar', array('id' => get_the_author_meta('ID')))
    </div>
    @include('partials.page-header')
  </div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php the_post() @endphp
    <article @php post_class() @endphp>
      <header>
        @include('partials/entry-image')
        <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
      </header>
      <div class="entry-summary">
        <div class="mb-2">
          @php // TODO: Make partial (part of entry-meta) @endphp
          <p class="byline author vcard inline-block">
              {{ __( 'Written by', 'sage') }} <span>{{ get_the_author_meta('display_name', get_the_ID()) }}</span>
              {{ __('in', 'sage') }}
              @php
                  $filter_categories = get_the_category();
                  $filter_categories_reverse = array_reverse($filter_categories);
                  $category = array_pop( $filter_categories_reverse );
                  $category_link = get_category_link( $filter_categories[0]->term_id );
                @endphp
                 <a
                 class="dd-entry-meta__category-link"
                 href="{{ $category_link }}">{{ $category->name }}</a>
            </p>
            <time class="inline-block text-xl" datetime="{{ get_post_time('c', true) }}">{{ __( 'on', 'sage' ) . ' ' . get_the_date() }}</time>
          <p class="inline-block">/ <span>{{ App\dd_reading_time(get_the_ID()) }}</span> <span>{{ __('Read time', 'sage') }}</span></p>
        </div>
        @php the_excerpt() @endphp
        <button class="mt-2 dd-btn dd-btn--outline"><a href="{{ get_the_permalink() }}">{{ __('Read more', 'sage') }}</a></button>
      </div>
    </article>
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
