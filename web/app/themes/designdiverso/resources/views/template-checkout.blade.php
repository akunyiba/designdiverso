{{--
  Template Name: Checkout
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.checkout-form')
  @endwhile
@endsection
