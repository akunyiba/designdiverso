// Import dependencies
import 'smoothstate/src/jquery.smoothState';
import { stripeGetScript } from '../util/helpers';
import { mauticGetScript } from '../util/helpers';

(function ($) {
  $(document).ready(function () {
    var options = {
      prefetch: true,
      // debug: true,
      blacklist: 'form, .no-smoothState, .shared-counts-button, #wpadminbar a, [data-plan-id]',
      forms: 'form',
      scroll: true,
      cacheLength: 4,
      onStart: {
        duration: 500, // Duration of our animation, if animations are longer than this, it causes problems
        render: function ($container) {
          // Add your CSS animation reversing class
          $container.addClass('is-exiting');
          $('.loading-overlay').fadeIn();

          // Enable body scrolling again when coming from a mobile modal menu
          $('body').removeClass('dd-modal-open');

          // Restart your animation.
          //If you looked at the authors demo, it uses .toggleAnimationClass(), which is outdated now
          //The Smoothstate demo on the authors site is outdated, but his Github is up to date and uses this method now.
          smoothState.restartCSSAnimations();
          
        },
      },
      onReady: {
        duration: 0,
        render: function ($container, $newContent) {
          // Remove your CSS animation reversing class
          $container.removeClass('is-exiting');
          $('.loading-overlay').fadeOut();

          // Inject the new content
          $container.html($newContent);
        },
      },
      onAfter: function () {
        mauticGetScript();
        stripeGetScript();
      },
    },
      smoothState = $('#smoothBody').smoothState(options).data('smoothState');
  });
})(jQuery);
