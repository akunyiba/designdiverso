import { getUrlParameter } from '../util/helpers';

(function ($) {
  $(function() {
    var mautic_services_form = $('.dd-mautic-services-form--select');
    var mautic_services_form_hidden_rows = mautic_services_form.find('.dd-mautic-row-hidden');
    var service = getUrlParameter('service');

    mautic_services_form_hidden_rows.hide();
    mautic_services_form.find('.mauticform-selectbox').change(function () {
      if ($(this).val()) {
        mautic_services_form_hidden_rows.slideDown();
      }
      else {
        mautic_services_form_hidden_rows.slideUp();
      }
    });

    $('a[href^="#mautic"]').click(function () {

      $('html, .dd-body').animate({
        scrollTop: mautic_services_form.offset().top,
      }, 500);

      mautic_services_form_hidden_rows.slideDown();

      var target = $(this).attr('href');

      mautic_services_form.find('.mauticform-selectbox option').each(function () {
        var text = '#mautic-' + $(this).text().replace(/\s+/g, '-').toLowerCase();
        if (text == target) {
          $(this).parent().val($(this).val());
        }
      });

      return false;
    });

    if (service) {
      mautic_services_form.find('.mauticform-selectbox option').each(function () {
        var text = $(this).text().replace(/\s+/g, '-').replace('&', 'and').toLowerCase();

        if (text == service.replace('&', 'and')) {
          $(this).parent().val($(this).val());
        }
      });
      mautic_services_form_hidden_rows.slideDown();
    }
  });
})(jQuery);
