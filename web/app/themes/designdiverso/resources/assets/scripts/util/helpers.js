export function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
}

export function mauticGetScript() {
  $.getScript('https://mautic.designdiverso.com/media/js/mautic-form.js', function () {
    // script is now loaded and executed.
    // put your dependent JS here.
    if (typeof MauticSDKLoaded == 'undefined') {

      var MauticSDKLoaded = true;
      var head = document.getElementsByTagName('head')[0];
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://mautic.designdiverso.com/media/js/mautic-form.js';
      script.onload = function () {
        window.MauticSDK.onLoad();
      };
      head.appendChild(script);
      var MauticDomain = 'https://mautic.designdiverso.com';
      var MauticLang = {
        'submittingMessage': 'Please wait...',
      }

      // is assigned a value but never used - eslint no unused vars
      if (MauticDomain) {
        //
      }
      else if (MauticLang) {
        //
      }
    }
  });
}

export function stripeGetScript() {
  $.getScript('https://js.stripe.com/v3/');
}

export function viewportGetHeight(){
  // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
  let vh = window.innerHeight * 0.01;

  // Then we set the value in the --vh custom property to the root of the document
  document.documentElement.style.setProperty('--vh', `${vh}px`);
}
