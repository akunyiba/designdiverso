export default {
  init() {
    // JavaScript to be fired on the home page

    /* eslint-disable */
    // TODO: Temporary, the shortcode duplicate
    $(function(){
      var config = {
          profile : {'screenName': 'designdiverso'},
          domId: 'dd-twitter-feed',
          maxTweets: 1,
          enableLinks: true,
          showUser: true,
          showTime: true,
          showRetweet: false,
          showInteraction: false,
          showImages: true,
          linksInNewWindow: true,
          lang: 'en',
          showPermalinks: true,
      };
      twitterFetcher.fetch(config);
    });
    /* eslint-enable */
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
