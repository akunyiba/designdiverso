// Import dependencies
import 'jquery-modal/jquery.modal';
import Headroom from 'headroom.js/dist/headroom'
import Glide from '@glidejs/glide'
/* eslint-disable */
import flatpickr from 'flatpickr'
/* eslint-enable */

import { mauticGetScript } from '../util/helpers';
import { viewportGetHeight } from '../util/helpers';

export default {
  init() {

    // Flatpickr
    var mauticFormDate = $('.mauticform-date input');
    // var maxDate = new Date();
    // maxDate.setMonth(maxDate.getMonth() + 2);
    // var maxdate_dd = String(maxDate.getDate()).padStart(2, '0');
    // var maxdate_mm = String(maxDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var maxdate_yyyy = maxDate.getFullYear();
    // maxDate = maxdate_yyyy + '-' + maxdate_mm + '-' + maxdate_dd;

    var today = new Date();
    today.setMonth(today.getMonth() + 1);
    var today_dd = String(today.getDate()).padStart(2, '0');
    var today_mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var today_yyyy = today.getFullYear();
    today = today_yyyy + '-' + today_mm + '-' + today_dd;

    mauticFormDate.flatpickr({disableMobile: 'true', minDate: today });

    // Plans
    $('a[data-plan-id]').click(function () {
      localStorage.setItem('plan_id', $(this).data('plan-id'));
    });

    // Plans accordion
    $('.dd-plans-column-accordion__item').click(function () {
      $(this).children('.dd-plans-column-accordion__item__text').slideToggle('fast');
      $(this).toggleClass('dd-plans-column-accordion__item--selected');
    });

    // Calculate the viewport height
    viewportGetHeight();

    // Load Stripe
    mauticGetScript();

    // Cookies
    window.addEventListener('load', function () {
      window.cookieconsent.initialise({
        'palette': {
          'popup': {
            'background': '#333130',
            'text': '#ffffff',
          },
          'button': {
            'background': 'transparent',
            'text': '#ffffff',
            'border': '#ffffff',
          },
        },
        'position': 'bottom-left',
        'content': {
          'message': 'This site uses cookies for an improved user experience.',
          'dismiss': 'OK',
          'link': 'Learn more',
          'href': 'https://www.designdiverso.com/privacypolicy.pdf',
        },
      })
    });

    // Headroom
    const header = document.querySelector('.banner');
    const headroom = new Headroom(header, {
      offset: 70,
    });
    headroom.init();

    // Smoothscroll
    $('a[href^="#"]').filter(':not(a[href^="#mautic"])').click(function () {
      $('html, .dd-body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top,
      }, 500, 'linear');
      return false;
    });
    $('a[href^="#subscribe"]').click(function () {
      $('html, .dd-body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top,
      }, 1500, 'linear');
      return false;
    });

    // Hamburger
    window.addEventListener('resize', () => {
      viewportGetHeight();
    });

    $('.dd-hamburger').click(function () {
      $(this).toggleClass('dd-hamburger--active');
      $('body').toggleClass('dd-modal-open');

      if ($(this).hasClass('dd-hamburger--active')) {
        $('.nav-primary').addClass('dd-hamburger--active');
      }
      else {
        $('.nav-primary').removeClass('dd-hamburger--active');
      }
    });

    // Modal windows
    $('a[data-modal]').click(function () {
      $(this).modal({
        fadeDuration: 250,
        fadeDelay: 0.80,
      });
      return false;
    });

    $(document).on($.modal.OPEN, function () {
      $('.close-modal').click(function () {
        jQuery.modal.close();
      });
    });

    // Glide sliders
    if ($('.dd-clients-carousel').length) {
      new Glide('.dd-clients-carousel', {
        type: 'carousel',
        autoplay: 4000,
        perView: 4,
        gap: 40,
        breakpoints: {
          576: {
            autoplay: 4000,
            perView: 2,
          },
        },
      }).mount();
    }

    if ($('.recent-posts').length) {
      new Glide('.recent-posts', {
        type: 'carousel',
        perView: 3,
        gap: 40,
        breakpoints: {
          576: {
            perView: 2,
          },
        },
      }).mount();
    }

    // Custom link effects
    $('.dd-cl-effect-4').each(function(){
      $(this).find('a').wrap('<span class="cl-effect-4"></span>')
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
