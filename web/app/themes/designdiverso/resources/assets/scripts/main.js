// Import external dependencies
import 'jquery';
import 'jquery-validation/dist/jquery.validate';

// Import everything from autoload
import './autoload/**/*'

// Cookies Script
import './cookies';

// Alpine
import 'alpinejs';

// Anime
import './anime';

// Twitter Fetcher
import './twitter-fetcher';

// Import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
