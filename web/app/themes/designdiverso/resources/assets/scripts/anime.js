import 'waypoints/lib/jquery.waypoints';
import 'waypoints/lib/shortcuts/inview';
import anime from 'animejs/lib/anime.es.js';

// Moving letters
anime.timeline({ loop: 1 }).add({
  targets: '.home .ml3 .letter', // TODO: relation to hero.blade.php
  opacity: [0, 1],
  easing: 'easeInOutQuad',
  duration: 1250,
  delay: (el, i) => 50 * (i + 1),
});

// Post listing
window.postListing = function() {
  return {
    category: false,
    menu: false,
    categoryAll() {
      if( this.menu ) {
        this.category = false;
      }
      else {
        this.menu = !this.menu;
      }
    },
    restarnAnime() {
      var $anime_slide_up = $('.dd-anime-slide-up');
      $anime_slide_up.each(function () {
      /* eslint-disable */
      new Waypoint.Inview({
        element: this,
        enter: function (direction) {
        },
        entered: function (direction) {
          anime({
            targets: this.element,
            translateY: [40, 0],
            duration: 2500,
            delay: 200,
            direction: 'normal',
            easing: 'spring(1, 80, 10, 0)',
          });
        },
        exit: function (direction) {
          // anime({
          //   targets: this.element,
          //   translateY: [40, 0],
          //   duration: 2500,
          //   delay: 200,
          //   direction: 'reverse',
          //   easing: 'spring(1, 80, 10, 0)',
          // });
        },
        exited: function (direction) {
        },
      });
      /* eslint-enable */
      });
    },
  }
}

window.almComplete = function(alm){
  var $anime_slide_up = $(alm.listing).find('.dd-anime-slide-up');

  $anime_slide_up.each(function(){
    if(!$(this).parent('.dd-anime-slide-up-container').length) {
      $(this).wrap('<div class="dd-anime-slide-up-container overflow-hidden mb-4"></div>');
    }
      /* eslint-disable */
    new Waypoint.Inview({
    element: this,
    enter: function (direction) {

    },
    entered: function (direction) {
      anime({
        targets: this.element,
        translateY: [40, 0],
        duration: 2500,
        delay: 200,
        direction: 'normal',
        easing: 'spring(1, 80, 10, 0)',
      });
    },
    exit: function (direction) {
      anime({
        targets: this.element,
        translateY: [40, 0],
        duration: 2500,
        delay: 200,
        direction: 'reverse',
        easing: 'spring(1, 80, 10, 0)',
      });
    },
    exited: function (direction) {

    },
  });
  /* eslint-enable */
  });
};

// Slide up
function animeSlideUp() {
  var $anime_slide_up = $('.dd-anime-slide-up');
  $anime_slide_up.each(function () {
    if(!$(this).parent('.dd-anime-slide-up-container').length) {
      $(this).wrap('<div class="dd-anime-slide-up-container overflow-hidden mb-4"></div>');
    }
    /* eslint-disable */
    new Waypoint.Inview({
      element: this,
      enter: function (direction) {

      },
      entered: function (direction) {
        anime({
          targets: this.element,
          translateY: [40, 0],
          duration: 2500,
          delay: 200,
          direction: 'normal',
          easing: 'spring(1, 80, 10, 0)',
        });
      },
      exit: function (direction) {
        // anime({
        //   targets: this.element,
        //   translateY: [40, 0],
        //   duration: 2500,
        //   delay: 200,
        //   direction: 'reverse',
        //   easing: 'spring(1, 80, 10, 0)',
        // });
      },
      exited: function (direction) {

      },
    });
    /* eslint-enable */
  });
}
animeSlideUp();

// Image animations
var anime_image = $('.dd-anime-image');
anime_image.each(function() {
  if( $(this).is( 'figure' ) )  {
    $(this).addClass('relative overflow-hidden');
    $(this).children('img').addClass('object-cover').wrap('<div class="dd-anime-image__image flex"></div>');
    $(this).append('<div class="dd-anime-image__bg w-full h-full bg-primary absolute z-negative" style="top: 0;"></div>');
  }
  else if( $(this).is( 'div' ) ) {
    $(this).children('figure').addClass('relative overflow-hidden');
    $(this).children('figure').children('img').addClass('object-cover').wrap('<div class="dd-anime-image__image flex"></div>');
    $(this).children('figure').append('<div class="dd-anime-image__bg w-full h-full bg-primary absolute z-negative" style="top: 0;"></div>');
  }
  /* eslint-disable */
  new Waypoint.Inview({
    element: this,
    offset: '50%',
    enter: function (direction) {
      // Create a timeline with default parameters
      var tl = anime.timeline({
        easing: 'linear',
        duration: 300,
        delay: 100,
      });
      // Add children
      tl
      .add({
        targets: this.element.querySelector('.dd-anime-image__bg'),
        translateX: ['-100%', '0%'],
      })
      .add({
        targets: this.element.querySelector('.dd-anime-image__image'),
        translateX: ['-100%', '0%'],
      });
    },
    entered: function (direction) {

    },
    exit: function (direction) {

    },
    exited: function (direction) {
      // Create a timeline with default parameters
      var tl = anime.timeline({
        easing: 'linear',
        duration: 200,
        delay: 100,
      });

      // Add children
      tl
      .add({
        targets: this.element.querySelector('.dd-anime-image__image'),
        translateX: ['0%', '-100%'],
        // direction: 'reverse',
      })
      .add({
        targets: this.element.querySelector('.dd-anime-image__bg'),
        translateX: ['0%', '-100%'],
        // direction: 'reverse',
      });
    },
  });
  /* eslint-enable */
});
